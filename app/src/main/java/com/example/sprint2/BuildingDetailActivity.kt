package com.example.sprint2

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class BuildingDetailActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    val cal = Calendar.getInstance()
    val day = cal.get(Calendar.DAY_OF_MONTH)
    val month = cal.get(Calendar.MONTH)+1
    val year = cal.get(Calendar.YEAR)
    val hour = cal.get(Calendar.HOUR_OF_DAY)
    val minute = cal.get(Calendar.MINUTE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_building_detail)
        val bundle: Bundle? = intent.extras
        val currentUser = bundle?.getString("user")
        val currentBuilding = bundle?.getString("building")

        //showAlert("User ${currentUser} is at $currentBuilding Building")

        setupContent(currentUser, currentBuilding.toString())

    }

    fun setupContent(user: String?, building: String) {
        val contentLayout = findViewById<LinearLayout>(R.id.occupancyLayout)
        //contentLayout.removeAllViews()

        db.collection("Buildings").get().addOnSuccessListener { result ->

            for (document in result) {
                var a: String = ""
                var data = document.data
                //showAlert(data.toString())
                val theName = data.get("Name").toString()
                val classrooms = data.get("classrooms") as List<*>
                if (theName == building) {
                    //var curCap = 0
                    for (c in classrooms) {
                        val room = c.toString().split("number=")[1].split(",")[0].toInt()
                        val currentCap = c.toString().split("currentCap=")[1].split("}")[0].toInt()
                        val maxCap = c.toString().split("maxCap=")[1].split(",")[0].toInt()
                        // curCap += thisCap
                        a = a + room



                        val card = LinearLayout(this)
                        val lpcard = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        lpcard.setMargins(80,50,80,10)

                        card.layoutParams = lpcard
                        card.orientation = LinearLayout.HORIZONTAL
                        card.removeAllViews()

                        val num = TextView(this)
                        val lpnum = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        //lpnum.setMargins(80,50,80,10)
                        lpnum.weight = 1.0f
                        num.text = ""+theName+" - "+room
                        num.layoutParams = lpnum

                        val cap = LinearLayout(this)
                        val lpcap = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        //lpcap.setMargins(80,50,80,10)
                        lpcap.weight = 1.0f
                        cap.orientation = LinearLayout.HORIZONTAL
                        cap.layoutParams = lpcap
                        cap.removeAllViews()

                        val capacity = TextView(this)
                        val lCapacity = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        //lCapacity.setMargins(80,50,80,10)
                        lCapacity.weight = 1.0f
                        capacity.text = ""+currentCap+" / "+maxCap
                        capacity.layoutParams = lCapacity

                        val person = ImageView(this)
                        val lPerson = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        //lCapacity.setMargins(80,50,80,10)
                        lPerson.weight = 1.0f
                        lPerson.gravity = Gravity.LEFT
                        person.background = resources.getDrawable(R.drawable.persona)

                        val button = Button(this)
                        val lButton = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        lButton.weight = 1.0f
                        button.layoutParams = lButton
                        button.text="Asistir"
                        button.setOnClickListener{
                            db.collection("reserves").document(user +"-"+ building + "-" + room+"-"+day+"-"+month+"-"+hour+"-"+minute).set(
                                hashMapOf(
                                    "day" to day, "month" to month, "year" to year, "hour" to hour, "minute" to minute, "room" to building + "-"+room, "name" to user)
                            )
                            showAlert("Acabas de confirmar tu reserva en "+building + "-"+room+ ". Confirma tu asistencia escaneando el QR.")

                        }


                        card.addView(num)
                        cap.addView(capacity)
                        //cap.addView(person)
                        card.addView(cap)
                        card.addView(button)
                        contentLayout.addView(card)
                    }

                }

            }

        }


    }


        fun showAlert(message: String) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Listo!")
            builder.setMessage(message)
            builder.setPositiveButton("Ok", null)
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

}