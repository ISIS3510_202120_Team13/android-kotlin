package com.example.sprint2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.sprint2.data.TheCache
import com.google.firebase.firestore.FirebaseFirestore

class SelfCareActivity : AppCompatActivity() {
    private val db = FirebaseFirestore.getInstance()
    val url1 = "https://www.who.int/images/default-source/health-topics/coronavirus/person-sick-in-your-household-prepare-es.tmb-479v.png?sfvrsn=6736e6e5_11"
    val url2 = "https://www.who.int/images/default-source/health-topics/coronavirus/person-sick-in-your-household-what-to-do-es.tmb-479v.png?sfvrsn=39d1287_12"
    val url3 = "https://www.who.int/images/default-source/health-topics/coronavirus/dont-put-off-medical-appointments-es.tmb-479v.png?sfvrsn=545773bb_11"
    val url4 = "https://www.who.int/images/default-source/health-topics/coronavirus/visiting-family-es.tmb-479v.png?sfvrsn=e4652390_11"
    val url5 = "https://www.who.int/images/default-source/health-topics/coronavirus/grocery-shopping-es.tmb-479v.png?sfvrsn=57603db5_11"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_self_care)


        supportActionBar?.setBackgroundDrawable(resources.getDrawable(R.drawable.profile))

        val actionBar = supportActionBar
        actionBar!!.title = Html.fromHtml("<font color= '#000000'>Información de cuidado</font>")

        val path1 = findViewById<ImageView>(R.id.info1)
        val path2 = findViewById<ImageView>(R.id.info2)
        val path3 = findViewById<ImageView>(R.id.info3)
        val path4 = findViewById<ImageView>(R.id.info4)
        val path5 = findViewById<ImageView>(R.id.info5)
        

        Glide.with(this).load(url1).error(R.drawable.fallback).into(path1)
        Glide.with(this).load(url2).error(R.drawable.fallback).into(path2)
        Glide.with(this).load(url3).error(R.drawable.fallback).into(path3)
        Glide.with(this).load(url4).error(R.drawable.fallback).into(path4)
        Glide.with(this).load(url5).error(R.drawable.fallback).into(path5)

        selfCareAnalytics()
    }

    fun selfCareAnalytics(){
        val user : String = TheCache.get("userLogin")
        db.collection("Users").document(user).get().addOnSuccessListener {
            val visited = it.get("visitedInfo")

            Log.d("FINAL BQ", "$visited")
            if(visited == null ){
                db.collection("Users").document(user).set(
                    hashMapOf(
                        "Faculty" to it.get("Faculty"),
                        "LastHW" to it.get("LastHW"),
                        "Login" to user,
                        "Name" to it.get("Name"),
                        "UniandesID" to it.get("UniandesID"),
                        "CurrentClassroom" to it.get("CurrentClassroom"),
                        "visitedInfo" to 1
                    )
                )
                db.collection("Analytics").document("visitInfo").get().addOnSuccessListener {
                    var ammount = it.get("ammountUsers").toString().toInt()
                    ammount = ammount + 1
                    db.collection("Analytics").document("visitInfo").set(
                        hashMapOf(
                            "ammountUsers" to ammount
                        )
                    )
                }

            }else{
                if (visited.toString().toInt() == 0){
                    db.collection("Users").document(user).set(
                        hashMapOf(
                            "Faculty" to it.get("Faculty"),
                            "LastHW" to it.get("LastHW"),
                            "Login" to user,
                            "Name" to it.get("Name"),
                            "UniandesID" to it.get("UniandesID"),
                            "CurrentClassroom" to it.get("CurrentClassroom"),
                            "visitedInfo" to 1
                        )
                    )
                    db.collection("Analytics").document("visitInfo").get().addOnSuccessListener {
                        var ammount = it.get("ammountUsers").toString().toInt()
                        ammount = ammount + 1
                        db.collection("Analytics").document("visitInfo").set(
                            hashMapOf(
                                "ammountUsers" to ammount
                            )
                        )
                    }
                }
            }
        }
    }

}