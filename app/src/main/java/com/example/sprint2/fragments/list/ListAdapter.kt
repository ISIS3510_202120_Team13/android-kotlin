package com.example.sprint2.fragments.list


import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.sprint2.R
import com.example.sprint2.ReservationDetail
import com.example.sprint2.data.Reservation
import kotlinx.android.synthetic.main.custom_row.view.*
import java.security.AccessController.getContext

class ListAdapter: RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    private var reservationList = emptyList<Reservation>()

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_row, parent, false))

    }

    override fun getItemCount(): Int {
        return reservationList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val currentItem = reservationList[position]
        holder.itemView.id_txt.text = currentItem.id.toString()
        holder.itemView.room.text = currentItem.room
        holder.itemView.rdate.text = ""+currentItem.day+"/"+currentItem.month+"/"+currentItem.year
        val btn = holder.itemView.button
        val c = holder.itemView.context
        btn.setText("Detalle")

        btn.setOnClickListener {
            if(isOnline(c)){
                val intent = Intent(c, ReservationDetail::class.java)
                intent.putExtra("Id", currentItem.id)
                intent.putExtra("Room", currentItem.room)
                intent.putExtra("Year", currentItem.year)
                intent.putExtra("Month", currentItem.month)
                intent.putExtra("Day", currentItem.day)
                intent.putExtra("Hour", currentItem.hour)
                intent.putExtra("Minute", currentItem.minute)
                intent.putExtra("user", "se.gamboa@uniandes.edu.co")

                startActivity(c, intent, null)
            }else{
                showAlert("Actualmente no hay conexión a internet, revisa tu red o inténtalo más tarde",c)
            }
        }
    }

    fun setData(reservation: List<Reservation>){
        this.reservationList = reservation
        notifyDataSetChanged()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    fun showAlert(message: String, context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Error!")
        builder.setMessage(message)
        builder.setPositiveButton("Ok",null)
        val dialog : AlertDialog = builder.create()
        dialog.show()
    }

}