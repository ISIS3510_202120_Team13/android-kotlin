package com.example.sprint2.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.sprint2.R
import com.example.sprint2.data.Cache
import com.google.firebase.firestore.FirebaseFirestore

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HWFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HWFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private val db = FirebaseFirestore.getInstance()
    var cache: Cache? = null

    var currentUser = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }



    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_h_w, container, false)
        currentUser = arguments?.getString("user").toString()
        cache = arguments?.getParcelable("cache") as Cache?
        val s = cache?.buildingInfo
        Log.d("CACHE", "Los buildings son: $s")
        val btnWashed = rootView.findViewById<Button>(R.id.justHW)
        val lastHw = rootView.findViewById<TextView>(R.id.lastHW)
        setLastHw(lastHw, false)
        btnWashed.setOnClickListener {
            newHW(lastHw)
        }

        // Inflate the layout for this fragment
        return rootView
    }

    fun newHW(lastHW: TextView){
        db.collection("Users").document(currentUser).get().addOnSuccessListener {
            db.collection("Users").document(currentUser).set(
                hashMapOf("Name" to it.get("Name"),
                    "UniandesID" to it.get("UniandesID"),
                    "Faculty" to it.get("Faculty"),
                    "Login" to it.get("Login"),
                    "classrooms" to it.get("classrooms"),
                    "LastHW" to System.currentTimeMillis().toString())
            )
        }
        setLastHw(lastHW, true)
    }

    fun setLastHw(lastHW:TextView, updated: Boolean){
        db.collection("Users").document(currentUser).get().addOnSuccessListener {
            val lastTime = it.get("LastHW").toString().toLong()
            val currentTime = System.currentTimeMillis()
            var deltaTime = (currentTime-lastTime)/60000

            lastHW.text = "You washed your hands $deltaTime minutes ago."

            if(updated) sendHWAnalytics(deltaTime)
        }
    }
    fun sendHWAnalytics(time: Long){
        db.collection("Analytics").document("averageHW").get().addOnSuccessListener {
            val currentAmmount = it.get("ammount").toString().toInt()
            val currentAverage = it.get("average").toString().toInt()
            Log.d("ANALYTICS","$currentAmmount reports. The average is $currentAverage")
            val newAmmount = currentAmmount+1
            val newAverage = (currentAmmount*currentAverage+time)/newAmmount
            Log.d("ANALYTICS","$newAmmount reports. The average is $newAverage")
            db.collection("Analytics").document("averageHW").set(
                hashMapOf("ammount" to newAmmount,
                    "average" to newAverage)
            )

        }.addOnFailureListener {
            Log.d("ANALYTICS","Data could not be sent, check connection")
        }
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HWFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HWFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}