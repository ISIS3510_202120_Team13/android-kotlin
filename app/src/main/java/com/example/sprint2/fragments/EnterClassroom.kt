package com.example.sprint2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.sprint2.R
import com.google.firebase.firestore.FirebaseFirestore

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"



/**
 * A simple [Fragment] subclass.
 * Use the [EnterClassroom.newInstance] factory method to
 * create an instance of this fragment.
 */
class EnterClassroom : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private val db = FirebaseFirestore.getInstance()
    var currentUser = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_enter_classroom, container, false)
        currentUser = arguments?.getString("user").toString()

        val newRoom = rootView.findViewById<EditText>(R.id.room_to_enter).toString()
        val newRoomBtn = rootView.findViewById<Button>(R.id.enterRoom)

        newRoomBtn.setOnClickListener {
            enterRoom(newRoom)
        }

        return rootView
    }

    fun enterRoom(room: String?){

            db.collection("Users").document(currentUser).get().addOnSuccessListener {
                val classrooms = it.get("classrooms") as List<*>
                var inList = false
                for(c in classrooms){
                    Toast.makeText(this.context, "Room: ${c.toString()}", Toast.LENGTH_LONG).show()
                    if(c.toString().equals(room)){
                        inList = true
                    }
                }
                if(!inList){
                    val newList = arrayListOf(classrooms, room)
                    showAlert(newList.toString())
                    db.collection("Users").document(currentUser).set(
                        hashMapOf("Name" to it.get("Name"),
                            "UniandesID" to it.get("UniandesID"),
                            "Faculty" to it.get("Faculty"),
                            "Login" to it.get("Login"),
                            "classrooms" to arrayListOf(classrooms,room),
                            "LastHW" to it.get("LastHW"))
                    )
                }
            }
        
    }

    fun showAlert(message: String) {
        val builder = this.context?.let { AlertDialog.Builder(it) }
        if (builder != null) {
            builder.setTitle("Error!")
            builder.setMessage(message)
            builder.setPositiveButton("Ok",null)
            val dialog : AlertDialog = builder.create()
            dialog.show()
        }

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment EnterClassroom.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            EnterClassroom().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}