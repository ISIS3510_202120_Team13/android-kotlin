package com.example.sprint2.data

import android.util.LruCache

object TheCache {
    val cache : LruCache<String,String> = LruCache<String,String>(4)
    fun put(key: String, value: String?){
        cache.put(key,value)
    }
    fun get(key:String): String {
        return cache.get(key)
    }
}