package com.example.sprint2.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReservationViewModel(application: Application): AndroidViewModel(application) {

    val readAllData: LiveData<List<Reservation>>
    private val repository: ReservationRepository


    init{
        val reservationDao = ReservationDatabase.getDatabase(application).reservationDao()
        repository = ReservationRepository(reservationDao)
        readAllData = repository.readAllData
    }

    fun addReservation(reservation: Reservation){
        viewModelScope.launch(Dispatchers.IO){
            repository.addReservation(reservation)
        }
    }

    fun deleteReservation(reservation: Reservation){
        viewModelScope.launch(Dispatchers.IO){
            repository.deleteReservation(reservation)
        }
    }


}