package com.example.sprint2.data

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context

@Database(entities = [Reservation::class], version = 1, exportSchema = false)
abstract class ReservationDatabase: RoomDatabase() {

    abstract fun reservationDao(): ReservationDao

    companion object{
        @Volatile
        private var INSTANCE: ReservationDatabase? = null

        fun getDatabase(context: Context): ReservationDatabase{
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ReservationDatabase::class.java,
                    "reservations_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}