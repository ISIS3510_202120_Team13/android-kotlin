package com.example.sprint2.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ReservationDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addReservation(reservation: Reservation)

    @Query("SELECT * FROM reservations_table ORDER BY id ASC")
    fun readAllData(): LiveData<List<Reservation>>

    @Delete
    fun deleteReservation(reservation: Reservation): Void



}