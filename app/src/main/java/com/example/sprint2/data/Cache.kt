package com.example.sprint2.data

import android.app.ActivityManager
import android.content.Context
import android.os.Parcelable
import android.util.Log
import android.util.LruCache
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class Cache : Parcelable{
    var cache: LruCache<String, String> = LruCache<String,String>(10)
    var lastHW: Long = 0
    var buildingInfo: String = ""
    var user : String? = ""

    fun put(key: String, value: String){
        cache?.put(key,value)
    }
    fun get(key: String): String? {
        return cache?.get(key)
    }
    fun remove(key: String): String?{
        return cache?.remove(key)
    }
    fun setUserLogin(pUser: String?){
        cache.put("userLogin", pUser)
        user = pUser
    }

    fun getUserLogin() : String? {
        return user
    }

    fun getUserLogin2() : String? {
        return cache.get("userLogin")
    }
    fun setBuildings(buildings: String){
        Log.d("CACHE","Se va a guardar $buildings")
        buildingInfo = buildings
        Log.d("CACHE","La info de los edificios es: $buildingInfo")
    }


}