package com.example.sprint2.data

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "reservations_table")
data class Reservation (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val room: String,
    val day: Int,
    val month: Int,
    val year: Int,
    val hour: Int,
    val minute: Int
)