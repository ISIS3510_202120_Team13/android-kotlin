package com.example.sprint2.data

import androidx.lifecycle.LiveData

class ReservationRepository(private val reservationDao: ReservationDao) {

    val readAllData: LiveData<List<Reservation>> = reservationDao.readAllData()

    fun addReservation(reservation: Reservation){
        reservationDao.addReservation(reservation)
    }

    suspend fun deleteReservation(reservation: Reservation){
        reservationDao.deleteReservation(reservation)
    }



}