package com.example.sprint2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import org.w3c.dom.Text

class RegisterActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()

    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        auth = Firebase.auth
        goBack()

        register()

    }

    fun goBack() {
        val btnGoBack = findViewById<Button>(R.id.btnGoBack)
        btnGoBack.setOnClickListener{
            finish()

        }
    }
    fun register() {
        val btnRegister = findViewById<Button>(R.id.btnRegisterInRegister)
        btnRegister.setOnClickListener {
            val name = findViewById<EditText>(R.id.registerName)
            val id = findViewById<EditText>(R.id.registerID)
            val faculty = findViewById<EditText>(R.id.registerFaculty)
            val login = findViewById<EditText>(R.id.registerEmail)
            val pass = findViewById<EditText>(R.id.registerPassword)
            val rePass = findViewById<EditText>(R.id.registerRepeatPassword)

            if(name.text.isNotEmpty() && id.text.isNotEmpty() && faculty.text.isNotEmpty() && login.text.isNotEmpty() && pass.text.isNotEmpty() && rePass.text.isNotEmpty()){
                if(!login.text.contains("@uniandes.edu.co") || login.text.split("@").size > 2 || login.text.split("@")[0].equals("")){
                    //Send error: invalid email
                    showAlert("Error in Uniandes Login")
                }else{
                    if(pass.text.toString() !=rePass.text.toString()){
                        //Send error: passwords must match
                        showAlert("Password and Repeat Password must match")
                    }else if(pass.text.toString().length < 6){
                        showAlert("Password must be at least 6 characters long")
                    }
                    else{
                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(login.text.toString(),
                            pass.text.toString()
                        ).addOnCompleteListener{
                            if(it.isSuccessful){
                                showSuccess("User created Successfully")
                                 db.collection("Users").document(login.text.toString()).set(
                                    hashMapOf("Name" to name.text.toString(),
                                     "UniandesID" to id.text.toString(),
                                     "Faculty" to faculty.text.toString(),
                                     "Login" to login.text.toString(),
                                     "classrooms" to arrayListOf(""),
                                     "LastHW" to System.currentTimeMillis().toString(),
                                     "visitedInfo" to 0)
                                )
                            }else{
                                showAlert("An error ocurred while registering the user")
                            }
                        }
                    }
                }
            }else{
                //Send error: there must not be any empty fields
                showAlert("There must not be any empty fields")
            }

        }
    }

    fun showAlert(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error!")
        builder.setMessage(message)
        builder.setPositiveButton("Ok",null)
        val dialog : AlertDialog = builder.create()
        dialog.show()
    }
    fun showSuccess(message: String) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Success!")
            builder.setMessage(message)
            builder.setPositiveButton("Ok",null)
            val dialog : AlertDialog = builder.create()
            dialog.show()
        }
}