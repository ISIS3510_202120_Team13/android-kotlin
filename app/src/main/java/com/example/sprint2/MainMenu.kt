package com.example.sprint2

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.view.marginBottom
import androidx.core.view.marginTop
import androidx.fragment.app.Fragment
import com.example.sprint2.data.Cache
import com.example.sprint2.data.TheCache
import com.example.sprint2.fragments.EnterClassroom
import com.example.sprint2.fragments.HWFragment
import com.example.sprint2.fragments.QRFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.api.Distribution
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.reflect.Array
import java.util.*

class MainMenu : AppCompatActivity() {
    private val db = FirebaseFirestore.getInstance()
    private var cache: Cache? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        //Saves the logged user in currentUser
        val bundle: Bundle? = intent.extras
        val currentUser =  bundle?.getString("user")
        val cachee = intent.getParcelableExtra<Cache>("cache")
        cachee?.put("a","Una cosa")
        val s = cachee?.get("a")
        Log.d("CACHE","Se guardo $s")

        setDate()
        setOptionsBtn(currentUser)
        setDateBtn(currentUser)

        setupContent(currentUser, cachee)

        val enter_classroom = EnterClassroom()
        val hw_fragment = HWFragment()
        val qr_fragment = QRFragment()

        val bottom_navigation = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        bottom_navigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.hwReport -> openHWView( currentUser, cachee)
                R.id.bookRoom -> /*setupContent(currentUser, cache)*/setUpFragment(enter_classroom, currentUser, bundle)
                R.id.scanQr -> scanQR(currentUser)
                R.id.careInformation -> showSelfCareView()
            }
            true
        }

    }
    fun showSelfCareView(){
        val intent = Intent(this, SelfCareActivity::class.java).apply {}
        startActivity(intent)
    }
    fun openHWView(currentUser: String?, cachee: Cache?){
        val intent = Intent(this, HWActivity::class.java).apply {
            putExtra("user",currentUser)

            val s = TheCache.get("userLogin")
            Log.d("CACHE", "Se recupera $s en MainMenu")
            putExtra("cache", cachee)
        }
        startActivity(intent)
    }
    fun scanQR(user: String?){
        val qrIntent : Intent = Intent(this, QRActivity::class.java).apply{
            putExtra("user", user)
        }
        startActivity(qrIntent)
    }
    fun setUpFragment(frag: Fragment, usr: String?, bun: Bundle?){
        frag.arguments = bun
        makeCurrentFragment(frag,usr)
    }
    private fun makeCurrentFragment(fragment: Fragment, user: String?) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            commit()
        }

    @RequiresApi(Build.VERSION_CODES.M)
    fun setupContent(user: String?, cachee : Cache?){
        val contentLayout = findViewById<LinearLayout>(R.id.contentLayout)
        //contentLayout.removeAllViews()
        db.collection("Buildings")
            .get()
            .addOnSuccessListener { result ->
                var buildings: String = ""
                for (document in result) {
                    var data = document.data
                    //showAlert(data.toString())
                    val theName = data.get("Name").toString()
                    val maxCap = data.get("Capacity").toString().toInt()
                    val classrooms = data.get("classrooms") as List<*>
                    var curCap = 0
                    for(c in classrooms){
                        val thisCap = c.toString().split("currentCap=")[1].split("}")[0].toInt()
                        curCap+=thisCap
                    }
                    val occup = (100*curCap/maxCap)
                    val occupation = "${occup.toString()}%"
                    buildings +="$theName-$occup;"
                    var draw: Drawable


                    val card = LinearLayout(this)
                    val lpCard =LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                    lpCard.setMargins(50,15,50,15)
                    card.layoutParams = lpCard
                    card.orientation = LinearLayout.VERTICAL

                    card.background = when{
                        theName.toLowerCase().equals("au") -> resources.getDrawable(R.drawable.au)
                        theName.toLowerCase().equals("b") -> resources.getDrawable(R.drawable.b)
                        theName.toLowerCase().equals("c") -> resources.getDrawable(R.drawable.c)
                        theName.toLowerCase().equals("g") -> resources.getDrawable(R.drawable.g)
                        theName.toLowerCase().equals("k2") -> resources.getDrawable(R.drawable.k2)
                        theName.toLowerCase().equals("ll") -> resources.getDrawable(R.drawable.ll)
                        theName.toLowerCase().equals("m") -> resources.getDrawable(R.drawable.m)
                        theName.toLowerCase().equals("ml") -> resources.getDrawable(R.drawable.ml)
                        theName.toLowerCase().equals("o") -> resources.getDrawable(R.drawable.o)
                        theName.toLowerCase().equals("q") -> resources.getDrawable(R.drawable.q)
                        theName.toLowerCase().equals("r") -> resources.getDrawable(R.drawable.r)
                        theName.toLowerCase().equals("rga") -> resources.getDrawable(R.drawable.rga)
                        theName.toLowerCase().equals("rgd") -> resources.getDrawable(R.drawable.rgd)
                        theName.toLowerCase().equals("s1") -> resources.getDrawable(R.drawable.s1)
                        theName.toLowerCase().equals("sd") -> resources.getDrawable(R.drawable.sd)
                        theName.toLowerCase().equals("tx") -> resources.getDrawable(R.drawable.tx)
                        theName.toLowerCase().equals("w") -> resources.getDrawable(R.drawable.w)
                        theName.toLowerCase().equals("y") -> resources.getDrawable(R.drawable.y)
                        theName.toLowerCase().equals("z") -> resources.getDrawable(R.drawable.z)
                        else -> resources.getDrawable(R.drawable.elseimg)
                    }
                    card.removeAllViews()

                    val hor1 = LinearLayout(this)
                    val lpHor1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT)
                    lpHor1.setMargins(0,30,0,10)
                    hor1.layoutParams  = lpHor1
                    hor1.orientation = LinearLayout.HORIZONTAL
                    hor1.removeAllViews()

                    val name = TextView(this)
                    val lpname = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                    lpname.weight = 1.0f
                    name.gravity = Gravity.CENTER
                    name.setTextSize(TypedValue.COMPLEX_UNIT_SP,20F)
                    name.setTypeface(null, Typeface.BOLD)
                    name.layoutParams = lpname
                    name.text = theName

                    val sym = TextView(this)
                    val lpsym = LinearLayout.LayoutParams(35,
                        35)
                    //lpsym.weight = 2.0f

                    sym.layoutParams = lpsym
                    when(occup){
                        in 0..25 -> sym.background = resources.getDrawable(R.drawable.excellent)
                        in 26..50 -> sym.background = resources.getDrawable(R.drawable.good)
                        in 51..75 -> sym.background = resources.getDrawable(R.drawable.caution)
                        in 76..100 -> sym.background = resources.getDrawable(R.drawable.danger)
                    }



                    val percent = TextView(this)
                    val lpPer = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                    lpPer.weight = 2.0f
                    percent.layoutParams = lpPer
                    percent.text = occupation
                    percent.setTextSize(TypedValue.COMPLEX_UNIT_SP,20F)
                    percent.setTypeface(null, Typeface.BOLD)


                    val hor2 = LinearLayout(this)
                    val lpHor2 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                    lpHor2.setMargins(0,10,0,20)
                    hor2.layoutParams  = lpHor2
                    hor2.orientation = LinearLayout.HORIZONTAL
                    hor2.removeAllViews()



                    val btnDetail = Button(this)
                    val lpbtndet = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                    lpbtndet.weight = 1.0f
                    lpbtndet.setMargins(200,15,200,15)
                    btnDetail.layoutParams = lpbtndet
                    btnDetail.setBackgroundColor(Color.rgb(6,61,112))
                    btnDetail.setTextColor(Color.WHITE)
                    btnDetail.setOnClickListener {
                        if(isOnline(this)){
                            buildingDetail(user, theName)
                        }else{
                            showAlert("Parece que no hay conexión a internet, revisa tu red o inténtalo más tarde.")
                        }

                    }

                    btnDetail.text = "Detail"


                    hor1.addView(name)
                    hor1.addView(sym)
                    hor1.addView(percent)
                    hor2.addView(btnDetail)
                    card.addView(hor1)
                    card.addView(hor2)
                    contentLayout.addView(card)


                }
                cachee?.setBuildings(buildings)
                var s = cachee?.buildingInfo
                Log.d("CACHE","Buildings es:\n$s")
                Log.d("CACHE","Buildings og es:\n$buildings")
            }
            .addOnFailureListener { exception ->
                var buildingInfo = cache?.buildingInfo
                if (buildingInfo == ""){
                    Toast.makeText(this,"non funciona", Toast.LENGTH_LONG).show()
                }
            }


    }
    fun buildingDetail(user: String?, building: String?){
        val intent = Intent(this, BuildingDetailActivity::class.java).apply{
            putExtra("user", user)
            putExtra("building",building)
        }
        startActivity(intent)
    }
    fun showAlert(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error!")
        builder.setMessage(message)
        builder.setPositiveButton("Ok",null)
        val dialog : AlertDialog = builder.create()
        dialog.show()
    }
    fun setDate(){
        //Instancia del calendario
        val calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        val nMonth = month+1
        val dayWeek = calendar.get(Calendar.DAY_OF_WEEK)

        val btnFecha: Button = findViewById(R.id.fecha)
        btnFecha.setText("Hoy es el "+day+" del mes "+nMonth+" del año "+year)
    }
    fun setOptionsBtn(currentUser: String?) {
        //Botón para redireccionar a la actividad de opciones
        val menuOpciones: ImageView = findViewById<ImageView>(R.id.menuOpciones)
        menuOpciones.setOnClickListener{
            val intent = Intent(this, OptionsActivity::class.java).apply{
                putExtra("user", currentUser)
                putExtra("cache",cache)
            }
            startActivity(intent)
        }
    }
    fun setDateBtn(currentUser: String?){
        val btnDate: Button = findViewById(R.id.btnDatePicker)
        btnDate.setOnClickListener{
            val intent = Intent(this, DatePickerReservation::class.java)
            intent.putExtra("user", currentUser)
            startActivity(intent)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

}