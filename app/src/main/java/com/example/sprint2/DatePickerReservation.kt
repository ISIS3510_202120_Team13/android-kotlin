package com.example.sprint2

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import java.util.*


class DatePickerReservation : AppCompatActivity(){

    private lateinit var btnDatePicker: Button
    private lateinit var btnTimePicker: Button
    private lateinit var btnContinue: Button

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_date_picker_reservation)

        //Change action bar colors
        val actionBar = supportActionBar
        supportActionBar?.setBackgroundDrawable(resources.getDrawable(R.drawable.options))

        val bundle: Bundle? = intent.extras
        val currentUser =  bundle?.getString("user")

        actionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#B3EEF4")))
        actionBar!!.title = Html.fromHtml("<font color='#000000'>Reservar salón</font>")

        if (actionBar != null) {

            // Customize the back button
            actionBar.setHomeAsUpIndicator(R.drawable.arrow);

            // showing the back button in action bar
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        //Set buttons
        btnDatePicker = findViewById(R.id.btnDate)
        btnTimePicker = findViewById(R.id.btnHour)
        btnContinue = findViewById(R.id.btnContinue)

        //Set up calendar instance
        val cal = Calendar.getInstance()
        val day = cal.get(Calendar.DAY_OF_MONTH)
        val month = cal.get(Calendar.MONTH)
        val year = cal.get(Calendar.YEAR)
        val hour = cal.get(Calendar.HOUR_OF_DAY)
        val minute = cal.get(Calendar.MINUTE)

        val intent = Intent(this, ClassroomReservation::class.java)


        //Date picker listener
        btnDatePicker.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, nYear, nMonth, nDay ->
                val mMonth=nMonth+1
                btnDatePicker.setText(""+nDay +"/"+mMonth +"/"+nYear)
                intent.putExtra("Day", nDay)
                intent.putExtra("Month", mMonth)
                intent.putExtra("Year", nYear)
            }, year, month, day)
            dpd.show()
        }

        //Time picker listener
        btnTimePicker.setOnClickListener {
            val tpd = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, nHour, nMinute ->
                btnTimePicker.setText(""+nHour+":"+nMinute)
                intent.putExtra("user", currentUser)
                intent.putExtra("Hour", nHour)
                intent.putExtra("Minute", nMinute)
            }, hour, minute, true)
            tpd.show()
        }

        //Continue button listener
        btnContinue.setOnClickListener {

            if(isOnline(this)){
                startActivity(intent)
            }else{
                showAlert("Parece que no hay conexión a internet, revisa tu red o inténtalo más tarde.")
            }

        }



    }

    fun showAlert(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Ha ocurrido un error")
        builder.setMessage(message)
        builder.setPositiveButton("Ok",null)
        val dialog : AlertDialog = builder.create()
        dialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }


}