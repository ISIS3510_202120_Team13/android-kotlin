package com.example.sprint2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.example.sprint2.data.TheCache

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bienvenido)

        val buttonIngresar:Button = findViewById<Button>(R.id.buttonIngresar)
        buttonIngresar.setOnClickListener{
            Log.d("CACHE","Test")
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }
}