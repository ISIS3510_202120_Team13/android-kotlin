package com.example.sprint2

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toolbar
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.example.sprint2.data.Cache

class OptionsActivity : AppCompatActivity() {
    companion object{
        val USER_EMAIL ="USER_EMAIL"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setBackgroundDrawable(resources.getDrawable(R.drawable.options))

        setContentView(R.layout.opciones)

        //Saves the logged user in currentUser
        val bundle: Bundle? = intent.extras
        val currentUser =  bundle?.getString("user")
        val cache = intent.getParcelableExtra<Cache>("cache")
        val s = cache?.get("a")
        Log.d("CACHE","Se recupero $s en opciones")

        val actionBar = supportActionBar
        actionBar!!.title = Html.fromHtml("<font color='#000000'>Opciones</font>")

            // Customize the back button
            actionBar.setHomeAsUpIndicator(R.drawable.arrow);

            // showing the back button in action bar
            actionBar.setDisplayHomeAsUpEnabled(true);


        //Botón para redireccionar a la actividad de perfil
        val textView4: TextView = findViewById<TextView>(R.id.textView4)
        textView4.setOnClickListener{
            val intent = Intent(this, ProfileActivity::class.java)
            intent.putExtra("user", currentUser)
            startActivity(intent)
        }

        //Botón para redireccionar a la actividad del mapa de la universidad
        val textView5: TextView = findViewById<TextView>(R.id.textView5)
        textView5.setOnClickListener{
            val intent = Intent(this, MapaActivity::class.java)
            startActivity(intent)
        }

        //Botón para redireccionar a la actividad de política de privacidad
        val textView10: TextView = findViewById<TextView>(R.id.textView10)
        textView10.setOnClickListener{
            val intent = Intent(this, PrivacyActivity::class.java)
            startActivity(intent)
        }

        //Botón para redireccionar a la actividad de inicio de sesión
        val textView11: TextView = findViewById<TextView>(R.id.textView11)
        textView11.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }

}