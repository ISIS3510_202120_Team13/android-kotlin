package com.example.sprint2

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.LruCache
import android.widget.Button
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.example.sprint2.data.Cache
import com.example.sprint2.data.TheCache
import com.google.firebase.auth.FirebaseAuth
import java.io.Serializable

class LoginActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        register()
        login()

    }

    fun forgotPassword(view: android.view.View) {}
    fun login() {
        val btnLogin = findViewById<Button>(R.id.btnLogin)
        btnLogin.setOnClickListener{
            val email = findViewById<EditText>(R.id.loginEmail)
            val pass = findViewById<EditText>(R.id.loginPassword)
            var preferences = false
            if (getPrefEmail()?.isNotBlank() == true && getPrefPass()?.isNotBlank() == true && !email.text.isNotEmpty() && !pass.text.isNotEmpty()){
                email.setText(getPrefEmail())
                pass.setText(getPrefPass())
                preferences = true
            }
            if(email.text.isNotEmpty() && pass.text.isNotEmpty()){
                FirebaseAuth.getInstance().signInWithEmailAndPassword(email.text.toString(),
                    pass.text.toString()).addOnCompleteListener {
                  if(it.isSuccessful){
                      onSave(email.text.toString(), pass.text.toString())
                      proceedLogin(email.text.toString())
                  }else{
                      if(!isOnline(this)){
                          if(preferences){
                              proceedLogin(email.text.toString())
                          }else{
                              showAlert("An error ocurred, check your credentials")
                          }
                      }else{
                          showAlert("An error ocurred, check your credentials")
                      }

                  }
                }
            }else{
                showAlert("Both email and password are required")
            }
        }
    }
    fun proceedLogin(email: String){
        val MainMenuIntent : Intent = Intent(this, MainMenu::class.java).apply{
            val cache = createCache()
            //Log.d("CACHE","Cache should be created")
            putExtra("user", email)
            TheCache.put("userLogin", email)
            val s = TheCache.get("userLogin")
            Log.d("CACHE", "Hay $s")
            putExtra("cache",cache)
            //Log.d("CACHE","Cache should be in the bundle")
        }
        startActivity(MainMenuIntent)
    }

    fun createCache(): Cache{
        //Log.d("CACHE","Trying to create cache")
        val c = Cache()
        //Log.d("CACHE","Cache created, going to return it")
        return c
    }
    fun register() {
        val btnRegister = findViewById<Button>(R.id.btnRegisterInLogin)
        btnRegister.setOnClickListener{
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    fun showAlert(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error!")
        builder.setMessage(message)
        builder.setPositiveButton("Ok",null)
        val dialog : AlertDialog = builder.create()
        dialog.show()
    }

    fun onSave(email: String, pass: String){
        val pref = getPreferences(Context.MODE_PRIVATE)
        val editor = pref.edit()

        editor.putString("email", email)
        editor.putString("pass", pass)
        editor.apply()
    }

    fun getPrefEmail(): String? {
        val pref = getPreferences(Context.MODE_PRIVATE)
        return pref.getString("email", "")
    }

    fun getPrefPass(): String? {
        val pref = getPreferences(Context.MODE_PRIVATE)
        return pref.getString("pass", "")
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

}