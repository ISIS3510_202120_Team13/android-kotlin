package com.example.sprint2

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.text.Html
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.example.sprint2.data.TheCache
import com.google.firebase.firestore.FirebaseFirestore
import org.w3c.dom.Text

class ProfileActivity : AppCompatActivity() {
    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setBackgroundDrawable(resources.getDrawable(R.drawable.profile))

        setContentView(R.layout.perfil)

        val bundle: Bundle? = intent.extras
        val currentUser = TheCache.get("userLogin")
        val correo: TextView = findViewById<TextView>(R.id.textView8)

        if(currentUser!=null ||currentUser!=""){
            correo.setText(currentUser)
        }



        val actionBar = supportActionBar
        actionBar!!.title = Html.fromHtml("<font color='#000000'>Mi perfil</font>")

            actionBar.setHomeAsUpIndicator(R.drawable.arrow)

            // showing the back button in action bar
            actionBar.setDisplayHomeAsUpEnabled(true)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        //val navController = findNavController(R.id.fragmentContainerView)

        val textView2: TextView = findViewById<TextView>(R.id.reservas)
        textView2.setOnClickListener{
            //navController.navigateUp()
            val navController = navHostFragment.navController

            val graph = navController.navInflater.inflate(R.navigation.my_nav)
            graph.startDestination = R.id.listFragment
            navController.graph = graph
            navController.setGraph(graph, intent.extras)
            //NavigationUI.setupActionBarWithNavController(this, navController)
            //navController.navigate(R.id.listFragment)
        }


    }



    fun getPrefEmail(): String? {
        val pref = getPreferences(Context.MODE_PRIVATE)
        return pref.getString("email", "")
    }


    fun showAlert(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error!")
        builder.setMessage(message)
        builder.setPositiveButton("Ok",null)
        val dialog : AlertDialog = builder.create()
        dialog.show()
    }
}