package com.example.sprint2

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.sprint2.data.Reservation
import com.example.sprint2.data.ReservationViewModel
import com.example.sprint2.data.TheCache
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.reservation_detail.*
import java.util.*

class ReservationDetail: AppCompatActivity() {

    private lateinit var txt: TextView
    private lateinit var txt2: TextView
    private lateinit var txt3: TextView
    private lateinit var btn: Button
    private lateinit var btn1: Button

    private val db = FirebaseFirestore.getInstance()
    private lateinit var rViewModel: ReservationViewModel

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.reservation_detail)
        supportActionBar?.setBackgroundDrawable(resources.getDrawable(R.drawable.profile))

        val actionBar = supportActionBar
        actionBar!!.title = Html.fromHtml("<font color='#000000'>Cancelar Reserva</font>")

        actionBar.setHomeAsUpIndicator(R.drawable.arrow)

        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true)


        rViewModel = ViewModelProvider(this).get(ReservationViewModel::class.java)

        val id = getIntent().getSerializableExtra("Id")
        val room = getIntent().getSerializableExtra("Room")
        val day = getIntent().getSerializableExtra("Day")
        val month = getIntent().getSerializableExtra("Month")
        val year = getIntent().getSerializableExtra("Year")
        val hour = getIntent().getSerializableExtra("Hour")
        val minute = getIntent().getSerializableExtra("Minute")


        val currentUser = TheCache.get("userLogin")

        txt = findViewById(R.id.textViewRoom)
        txt2 = findViewById(R.id.textViewDate)
        txt3 = findViewById(R.id.textViewHour)
        txt.setText(""+room+" ")
        txt2.setText(""+day+"/"+month+"/"+year)
        txt3.setText(""+hour+":"+minute)

        btn = findViewById(R.id.buttonDelete)
        btn1 = findViewById(R.id.button2)
        btn.setOnClickListener {
            if(isOnline(this)){
                deleteReservation(
                    id as Int,
                    day as Int, month as Int, year as Int, hour as Int, minute as Int, room.toString())
                    showAlert("Has cancelado la reserva del salón "+ room+" para el día "+day+" del mes "+month+" del año "+year+" a las "+hour+":"+minute)
                    db.collection("reserves").document(""+currentUser+"-"+room+"-"+day+"-"+month+"-"+year+"-"+hour+"-"+minute).delete()
                    db.collection("Analytics").document("reserveCancelation-"+currentUser).get().addOnSuccessListener {
                    var currentAmmount = it.get("ammount").toString().toInt()
                    currentAmmount=currentAmmount+1
                    db.collection("Analytics").document("reserveCancelation-"+currentUser).set(hashMapOf(
                    "user" to currentUser, "ammount" to currentAmmount))
                }
            }else{
                showAlert("Actualmente no hay conexión a internet, revisa tu red o inténtalo más tarde")
            }
        }
        btn1.setOnClickListener {
            if(isOnline(this)){
                db.collection("Analytics").document("reserveCancelation-"+currentUser).get().addOnSuccessListener {
                    val currentAmmount = it.get("ammount").toString().toInt()

                    showAlert("Has cancelado un total de "+currentAmmount+" reservas")

                }
            }else{
                showAlert("Actualmente no hay conexión a internet, revisa tu red o inténtalo más tarde")
            }

        }
    }

    private fun deleteReservation(id: Int, d: Int, m: Int, y: Int, h: Int, min: Int, r: String){
        val reservation = Reservation(id, r, d, m, y, h, min)
        rViewModel.deleteReservation(reservation)
        //Toast.makeText(requireContext(), "Añadido correctamente", Toast.LENGTH_LONG).show()
    }

    fun showAlert(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    fun showAlert(message: String, context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Error!")
        builder.setMessage(message)
        builder.setPositiveButton("Ok",null)
        val dialog : AlertDialog = builder.create()
        dialog.show()
    }


}