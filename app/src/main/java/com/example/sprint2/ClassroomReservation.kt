package com.example.sprint2

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.ViewModelProvider
import com.example.sprint2.data.Reservation
import com.example.sprint2.data.ReservationViewModel
import com.google.firebase.firestore.FirebaseFirestore

class ClassroomReservation : AppCompatActivity() {

    private lateinit var button: Button
    private lateinit var button1: Button
    private lateinit var button2: Button
    private lateinit var button3: Button
    private lateinit var button4: Button
    private lateinit var button5: Button

    private lateinit var room: TextView
    private lateinit var room1: TextView
    private lateinit var room2: TextView
    private lateinit var room3: TextView
    private lateinit var room4: TextView
    private lateinit var room5: TextView

    private val db = FirebaseFirestore.getInstance()
    private lateinit var rViewModel: ReservationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_classroom_reservation)

        rViewModel = ViewModelProvider(this).get(ReservationViewModel::class.java)

        //Set buttons
        button = findViewById(R.id.btnReserve)
        button1 = findViewById(R.id.btnReserve1)
        button2 = findViewById(R.id.btnReserve2)
        button3 = findViewById(R.id.btnReserve3)
        button4 = findViewById(R.id.btnReserve4)
        button5 = findViewById(R.id.btnReserve5)

        //Set rooms
        room = findViewById(R.id.tvRoom)
        room1 = findViewById(R.id.tvRoom1)
        room2 = findViewById(R.id.tvRoom2)
        room3 = findViewById(R.id.tvRoom3)
        room4 = findViewById(R.id.tvRoom4)
        room5 = findViewById(R.id.tvRoom5)

        val bundle: Bundle? = intent.extras
        val currentUser =  bundle?.getString("user")

        //Change action bar colors
        val actionBar = supportActionBar
        supportActionBar?.setBackgroundDrawable(resources.getDrawable(R.drawable.options))

        actionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#B3EEF4")))
        actionBar!!.title = Html.fromHtml("<font color='#000000'>Reservar salón</font>")

        if (actionBar != null) {

            // Customize the back button
            actionBar.setHomeAsUpIndicator(R.drawable.arrow);

            // showing the back button in action bar
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        val day = getIntent().getSerializableExtra("Day")
        val month = getIntent().getSerializableExtra("Month")
        val year = getIntent().getSerializableExtra("Year")
        val hour = getIntent().getSerializableExtra("Hour")
        val minute = getIntent().getSerializableExtra("Minute")

        actionBar.setDisplayHomeAsUpEnabled(true)

        //val capacidad = findViewById<TextView>(R.id.tvCapacity1) as Int
        button1.setOnClickListener {
            db.collection("reserves").document(""+currentUser+"-"+room1.text+"-"+day+"-"+month+"-"+year+"-"+hour+"-"+minute).set(
                hashMapOf(
                    "day" to day, "month" to month, "year" to year, "hour" to hour, "minute" to minute, "room" to room1.text, "name" to currentUser)
            )
            insertDataToDatabase(day as Int, month as Int, year as Int, hour as Int, minute as Int, room1.text.toString())
            showAlert("Se ha realizado una reserva para el día "+day+" del mes "+month+" del año "+year+" a las "+hour+":"+minute)
            redirectToMain()
        }

        button.setOnClickListener {
            db.collection("reserves").document(""+currentUser+"-"+room.text+"-"+day+"-"+month+"-"+year+"-"+hour+"-"+minute).set(
                hashMapOf(
                    "day" to day, "month" to month, "year" to year, "hour" to hour, "minute" to minute, "room" to room.text, "name" to currentUser)
            )
            insertDataToDatabase(day as Int, month as Int, year as Int, hour as Int, minute as Int, room.text.toString())
            showAlert("Se ha realizado una reserva para el día "+day+" del mes "+month+" del año "+year+" a las "+hour+":"+minute)
            redirectToMain()
        }
        button2.setOnClickListener {
            db.collection("reserves").document(""+currentUser+"-"+room2.text+"-"+day+"-"+month+"-"+year+"-"+hour+"-"+minute).set(
                hashMapOf(
                    "day" to day, "month" to month, "year" to year, "hour" to hour, "minute" to minute, "room" to room2.text, "name" to currentUser)
            )
            insertDataToDatabase(day as Int, month as Int, year as Int, hour as Int, minute as Int, room2.text.toString())
            showAlert("Se ha realizado una reserva para el día "+day+" del mes "+month+" del año "+year+" a las "+hour+":"+minute)
            redirectToMain()
        }
        button3.setOnClickListener {
            db.collection("reserves").document(""+currentUser+"-"+room3.text+"-"+day+"-"+month+"-"+year+"-"+hour+"-"+minute).set(
                hashMapOf(
                    "day" to day, "month" to month, "year" to year, "hour" to hour, "minute" to minute, "room" to room3.text, "name" to currentUser)
            )
            insertDataToDatabase(day as Int, month as Int, year as Int, hour as Int, minute as Int, room3.text.toString())
            showAlert("Se ha realizado una reserva para el día "+day+" del mes "+month+" del año "+year+" a las "+hour+":"+minute)
            redirectToMain()
        }
        button4.setOnClickListener {
            db.collection("reserves").document(""+currentUser+"-"+room4.text+"-"+day+"-"+month+"-"+year+"-"+hour+"-"+minute).set(
                hashMapOf(
                    "day" to day, "month" to month, "year" to year, "hour" to hour, "minute" to minute, "room" to room4.text, "name" to currentUser)
            )
            insertDataToDatabase(day as Int, month as Int, year as Int, hour as Int, minute as Int, room4.text.toString())
            showAlert("Se ha realizado una reserva para el día "+day+" del mes "+month+" del año "+year+" a las "+hour+":"+minute)
            redirectToMain()
        }
        button5.setOnClickListener {
            db.collection("reserves").document(""+currentUser+"-"+room5.text+"-"+day+"-"+month+"-"+year+"-"+hour+"-"+minute).set(
                hashMapOf(
                    "day" to day, "month" to month, "year" to year, "hour" to hour, "minute" to minute, "room" to room5.text, "name" to currentUser)
            )
            insertDataToDatabase(day as Int, month as Int, year as Int, hour as Int, minute as Int, room5.text.toString())
            showAlert("Se ha realizado una reserva para el día "+day+" del mes "+month+" del año "+year+" a las "+hour+":"+minute)
            redirectToMain()
        }





    }

    private fun insertDataToDatabase(d: Int, m: Int, y: Int, h: Int, min: Int, r: String){

        val reservation = Reservation(0, r, d, m, y, h, min)
        rViewModel.addReservation(reservation)
        //Toast.makeText(requireContext(), "Añadido correctamente", Toast.LENGTH_LONG).show()
    }

    private fun redirectToMain(){
        Handler().postDelayed({
            val intent = Intent(this, MainMenu::class.java)
            startActivity(intent)
        }, 2000)
    }


    fun showAlert(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}


