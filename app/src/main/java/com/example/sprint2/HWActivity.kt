package com.example.sprint2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_h_w.*
import com.example.sprint2.data.Cache
import com.example.sprint2.data.TheCache

class HWActivity : AppCompatActivity() {
    private val db = FirebaseFirestore.getInstance()
    var currentUser = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hwactivity)

        supportActionBar?.setBackgroundDrawable(resources.getDrawable(R.drawable.profile))

        val actionBar = supportActionBar
        actionBar!!.title = Html.fromHtml("<font color= '#000000'>Lavado de manos</font>")

        val bundle : Bundle? = intent.extras
        currentUser = bundle?.getString("user").toString()
        val cache = intent.getParcelableExtra<Cache>("cache")
        val s = TheCache.get("userLogin")
        Log.d("CACHE", "Se recupera $s en HWAct")

        val lastHWTV = findViewById<TextView>(R.id.lastHWReportText)
        showLastHW(lastHWTV, false)
        val btnHW = findViewById<Button>(R.id.btnReportLastHW)
        btnHW.setOnClickListener{
            updateLastHW(lastHWTV)

        }

    }
    fun updateLastHW(lastHW: TextView){
        db.collection("Users").document(currentUser).get().addOnSuccessListener {
            val currentTime = System.currentTimeMillis().toString()
            db.collection("Users").document(currentUser).set(
                hashMapOf("Name" to it.get("Name"),
                "UniandesID" to it.get("UniandesID"),
                "Faculty" to it.get("Faculty"),
                "Login" to it.get("Login"),
                "classrooms" to it.get("classrooms"),
                "LastHW" to currentTime)
            )
            TheCache.put("lastHW", currentTime)
        }.continueWith {
            showLastHW(lastHW, true)
        }

    }
    fun showLastHW(lastHW: TextView, updated: Boolean){
        db.collection("Users").document(currentUser).get().addOnSuccessListener {
            val lastTime = it.get("LastHW").toString().toLong()
            val currentTime = System.currentTimeMillis()
            var deltaTime = (currentTime - lastTime)/60000

            lastHW.text = "Tu último lavado de manos fue hace $deltaTime minutos."

            if(updated) sendHWAnalytics(deltaTime)
        }.addOnFailureListener {
            val lastTime : String? = TheCache.get("lastHW")
            if(lastTime != null){
                val lT = lastTime.toLong()
                val currentTime = System.currentTimeMillis()
                var deltaTime = (currentTime - lT)/60000

                lastHW.text = "Tu último lavado de manos fue hace $deltaTime minutos."
            }else{
                lastHW.text = "Ha ocurrido un error y no se ha podido recuperar tu último reporte."
            }
        }
    }

    fun sendHWAnalytics(time: Long){
        db.collection("Analytics").document("averageHW").get().addOnSuccessListener {
            val currentAmmount = it.get("ammount").toString().toInt()
            val currentAverage = it.get("average").toString().toInt()
            val newAmmount = currentAmmount+1
            val newAverage = (currentAmmount*currentAverage + time)/newAmmount
            db.collection("Analytics").document("averageHW").set(
                hashMapOf("ammount" to newAmmount,
                "average" to newAverage)
            )
        }
    }

}